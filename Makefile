PACKAGE=$(shell echo *.spec | sed -e 's/.spec$$//')

all: srpm sources

sources: $(PACKAGE).spec
	spectool -g $(PACKAGE).spec
	cp patches/* .

srpm: sources $(PACKAGE).spec
	rpmbuild -D '_srcrpmdir ./' -D '_sourcedir ./' -bs $(PACKAGE).spec

clean:
	rm $(PACKAGE)-*.tar.gz
	ls patches/ | xargs rm -f
	rm -f $(PACKAGE)-*.src.rpm
